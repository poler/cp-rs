extern crate clap;

#[macro_use]
extern crate log;

extern crate stderrlog;

mod cp;

use clap::{App, Arg};

use std::process;

// -i prompt before overwriting existing file
// -p preserve permissions

// if_cfg macros for unix only or libc only if appropriate

// rename tests, they look horrible
// - look up idiomatic test naming for rust

// TODO figure out logging in rust and configure it appropriately throughout
// instead of verbosity if statements

// TODO see if annotations can be used to do setup in tests

fn main() {
  let matches = App::new("Copy files and shit")
    .version("0.1")
    .author("Jon Poler <jonathan.poler@gmail.com>")
    .about("Unix-like copy but strictly worse")
    .arg(Arg::with_name("recursive")
         .short("R")
         .long("recursive")
         .help("Copies a directory recursively"))
    .arg(Arg::with_name("no-overwrite")
         .short("n")
         .long("no-overwrite")
         .help("Do not overwrite existing files"))
    .arg(Arg::with_name("follow-symlinks")
         .short("L")
         .long("dereference")
         .help("Follow symlinks"))
    .arg(Arg::with_name("v")
         .short("v")
         .multiple(true)
         .help("Sets the level of verbosity"))
    .arg(Arg::with_name("src")
         .help("Sets the source file")
         .required(true)
         .value_name("SOURCE")
         .multiple(true))
    .arg(Arg::with_name("dst")
         .help("Sets the destination file")
         .required(true)
         .value_name("DEST"))
    .get_matches();

  let src_paths: Vec<_> = matches.values_of("src").unwrap().collect();
  let dst_path = matches.value_of("dst").unwrap();
  let is_recursive = matches.is_present("recursive");
  let overwrite = !matches.is_present("no-overwrite");
  let follow_symlinks = matches.is_present("follow-symlinks");
  let verbosity = matches.occurrences_of("v");
  println!("module_path: {}", module_path!());

  stderrlog::new()
    .module(module_path!())
    // .quiet(opt.quiet)
    .verbosity(verbosity as usize)
    // .timestamp(opt.ts.unwrap_or(stderrlog::Timestamp::Off))
    .modules(vec![format!("{}::cp", module_path!())])
    .init()
    .unwrap();

  let runner = cp::Runner::new(cp::Config{
    src_paths: src_paths,
    dst_path: dst_path,
    is_recursive: is_recursive,
    overwrite: overwrite,
    follow_symlinks: follow_symlinks,
  });

  if let Err(_) = runner.run() {
    process::exit(1);
  }
}
