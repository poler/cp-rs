extern crate tempdir;

extern crate stderrlog;

extern crate libc;

use super::*;

use std::{
  io,
  path,
  fs,
  hash,
  collections::{self, hash_map::DefaultHasher},
  process,
  sync::{Once, ONCE_INIT}
};

static INIT: Once = ONCE_INIT;

macro_rules! test_setup {
  ($( $name:ident, $blk:block ) ,* ) => {
    $(
      #[test]
      fn $name() {
        INIT.call_once(|| setup());
        $blk
      }
    )*
  }
}

fn setup() {
  stderrlog::new()
    .module(module_path!())
  // can be changed to true to look at test output. Annoyingly, rust tests do
  // not properly capture stderr.
    .quiet(true)
    .verbosity(4)
    .modules(vec!["cp_rs::cp"])
    .init()
    .unwrap();
}

fn print_tree<P: AsRef<path::Path>>(path: P) {
  let output = process::Command::new("find")
    .arg(path.as_ref().to_str().unwrap())
    .arg("-ls")
    .output()
    .expect("executing find failed");

  assert_eq!(output.status.success(), true);
  println!("tree output:\n{}", String::from_utf8_lossy(&output.stdout));
}

struct FsLayout<D, F, S> {
  dirs: D,
  files: F,
  symlinks: S,
}

impl<'a, P, D, F, S> FsLayout<D, F, S> where
  P: 'a + AsRef<path::Path>,
  D: IntoIterator<Item = &'a P>,
  F: IntoIterator<Item = &'a (P, &'static str)>,
  S: IntoIterator<Item = &'a (P, P)>
{
  fn new(dirs: D, files: F, symlinks: S) -> FsLayout<D, F, S> {
    FsLayout{
      dirs: dirs,
      files: files,
      symlinks: symlinks,
    }
  }

  fn build<B: AsRef<path::Path>>(self, base_dir: B) -> Result<()> {
    for dir in self.dirs {
      fs::create_dir(base_dir.as_ref().join(dir))?;
    }
    for (file, contents) in self.files {
      fs::write(base_dir.as_ref().join(file), contents.as_bytes())?;
    }
    for (target, dst) in self.symlinks {
      // fiddle with changing this later
      symlinkat(&target.as_ref().to_path_buf(), &base_dir.as_ref().join(dst.as_ref()))?;
    }
    Ok(())
  }
}

struct WriteHasher<H>(H);

impl<H> WriteHasher<H> where
  H: hash::Hasher
{
  fn new(inner: H) -> WriteHasher<H> {
    WriteHasher(inner)
  }

  fn finish(&self) -> u64 {
    self.0.finish()
  }
}


impl<H> io::Write for WriteHasher<H> where
  H: hash::Hasher
{
  fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
    self.0.write(buf);
    Ok(buf.len())
  }

  fn flush(&mut self) -> io::Result<()> { Ok(()) }
}

fn hash_file<P: AsRef<path::Path>>(path: P) -> io::Result<u64> {
  let mut hasher = WriteHasher::new(DefaultHasher::new());
  let mut file = fs::File::open(path)?;
  io::copy(&mut file, &mut hasher)?;
  Ok(hasher.finish())
}



// test cycle that goes outside (above) base directory

// symlink edge cases:
// is relative to above src (an ancestor)
// is absolute and a descendant of src
// is absolute and completely outside of src and dst
// is a directory and is a cycle! (test this for both rec and non-rec)

// link to a link

// how to treat hard links in general?
// test hard link cycles


test_setup!(
  hash_file_eq_ne, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_f1 = tmp_dir_path.join("f1");
    let abs_f2 = tmp_dir_path.join("f2");
    let abs_f3 = tmp_dir_path.join("f3");
    let dirs: [&str; 0] = [];
    let files = [("f1", "foo"), ("f2", "foo"), ("f3", "bar")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir.path()).unwrap();

    assert_eq!(hash_file(&abs_f1).unwrap(), hash_file(&abs_f2).unwrap());
    assert_ne!(hash_file(&abs_f1).unwrap(), hash_file(&abs_f3).unwrap());
  },

  src_dir_not_recursive, {
    // rewrite with a nice setup routine and a structure perhaps
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let dirs = ["d1", "d2"];
    let files: [(&str, &'static str); 0] = [];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir.path()).unwrap();
    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: false,
      overwrite: false,
      follow_symlinks: false,
    });
    let mut err_vec = runner.run().unwrap_err();
    assert_eq!(err_vec.errors().len(), 1);
    err_vec.errors().iter().for_each(|err| {
      match err.repr {
        Repr::Simple(ErrorKind::NotRecursiveSrcIsDir) => {},
        _ => panic!("unexpected error"),
      }
    })
  },

  fail_dst_parent_non_existent, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2/d3");
    let abs_d3 = tmp_dir_path.join("d2/d3");
    let dirs = ["d1"];
    let layout = FsLayout::new(&dirs[..], vec![], vec![]);
    layout.build(tmp_dir.path()).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d3.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });

    let mut err_vec = runner.run().unwrap_err();
    let errors = err_vec.errors();
    assert_eq!(errors.len(), 1);
    // is there a more ergonomic way to do this?
    // the io_err has to be owned or the downcast_ref triggers the borrow checker
    // to complain about needing a static lifetime for the Any trait
    match errors.pop().unwrap().repr {
      Repr::Custom(custom) => {
        match *custom {
          Custom{
            kind: ErrorKind::IoError,
            error: err,
          } => {
            if let Some(io_err) = err.downcast_ref::<io::Error>() {
              assert_eq!(io_err.kind(), io::ErrorKind::NotFound);
            } else {
              panic!("unexpected error")
            }
          },
          _ => panic!("unexpected error")
        }
      },
      _ => panic!("unexpected error"),
    }

    assert_eq!(abs_d2.exists(), false);
    assert_eq!(abs_d3.exists(), false);
  },

  succeed_not_recursive, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_f1 = tmp_dir_path.join("f1");
    let abs_f2 = tmp_dir_path.join("f2");
    let dirs: [&str; 0] = [];
    let files = [("f1", "foo")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir.path()).unwrap();
    let runner = Runner::new(Config{
      src_paths: vec![abs_f1.to_str().unwrap()],
      dst_path: abs_f2.to_str().unwrap(),
      is_recursive: false,
      overwrite: false,
      follow_symlinks: false,
    });
    runner.run().unwrap();
    assert_eq!(abs_f2.exists() && abs_f2.is_file(), true);
    assert_eq!(hash_file(&abs_f1).unwrap(), hash_file(&abs_f2).unwrap());
  },

  succeed_dst_parent_exists, {
    // rewrite with a nice setup routine and a structure perhaps
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d3 = tmp_dir_path.join("d2/d3");
    let dirs = ["d1", "d2"];
    let files: [(&str, &'static str); 0] = [];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir.path()).unwrap();
    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d3.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });
    runner.run().unwrap();
    assert_eq!(abs_d3.exists() && abs_d3.is_dir(), true);
  },

  succeed_dfs_iterator, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let dirs = ["d1", "d1/d2"];
    let files = [("f1", "foo"), ("d1/f2", "bar"), ("d1/d2/f3", "baz")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);

    let mut expect_set: collections::HashSet<&str> = [
      "d1", "d1/d2", "f1", "d1/f2", "d1/d2/f3",
    ].iter().cloned().collect();
    layout.build(tmp_dir.path()).unwrap();
    for res in DfsPathIterator::from_path(tmp_dir_path, false).unwrap() {
      let path = res.unwrap().path();
      let suffix = path.strip_prefix(tmp_dir_path).unwrap();
      assert_eq!(expect_set.contains(suffix.to_str().unwrap()), true);
      assert_eq!(expect_set.remove(suffix.to_str().unwrap()), true);
    }

    assert_eq!(expect_set.len(), 0);
  },
  succeed_dfs_iterator_follow_symlinks, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let dirs = ["d1", "d1/d2"];
    let files = [("f1", "foo")];
    let symlinks = [("f1", "l1"), ("d1/d2", "l2")];
    let layout = FsLayout::new(&dirs[..], &files[..], &symlinks[..]);
    layout.build(tmp_dir.path()).unwrap();

    print_tree(tmp_dir_path);

    let mut count_map: collections::HashMap<u64, u64> = collections::HashMap::new();
    for res in DfsPathIterator::from_path(tmp_dir_path, true).unwrap() {
      let ino = fs::symlink_metadata(res.unwrap().path()).unwrap().st_ino();
      count_map.entry(ino)
        .and_modify(|e| *e += 1)
        .or_insert(1);
    }


    let f1_ino = fs::symlink_metadata(tmp_dir_path.join("f1")).unwrap().st_ino();
    let d2_ino = fs::symlink_metadata(tmp_dir_path.join("d1/d2")).unwrap().st_ino();
    let l1_ino = fs::symlink_metadata(tmp_dir_path.join("l1")).unwrap().st_ino();
    let l2_ino = fs::symlink_metadata(tmp_dir_path.join("l2")).unwrap().st_ino();
    println!("count map: {:?}", count_map);
    assert_eq!(count_map.get(&f1_ino), Some(&1));
    assert_eq!(count_map.get(&d2_ino), Some(&1));
    assert_eq!(count_map.get(&l1_ino), Some(&1));
    assert_eq!(count_map.get(&l2_ino), Some(&1));
  },

  fail_dfs_iterator_follow_symlinks_cycle, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let dirs = ["d1", "d1/d2"];
    let symlinks = [("..", "d1/l1"), ("../..", "d1/d2/l2")];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir.path()).unwrap();

    print_tree(tmp_dir_path);

    let mut count_map: collections::HashMap<u64, u64> = collections::HashMap::new();
    for res in DfsPathIterator::from_path(tmp_dir_path, true).unwrap() {
      let dir_entry = res.unwrap();
      // println!("'{}'", dir_entry.path().display());
      let ino = dir_entry.path().metadata().unwrap().st_ino();
      count_map.entry(ino)
        .and_modify(|e| *e += 1)
        .or_insert(1);
    }

    let d1_ino = tmp_dir_path.join("d1").metadata().unwrap().st_ino();
    assert_eq!(count_map.get(&d1_ino), Some(&1));
  },

  symlink_forward_reference_cross_subtrees_no_cycle, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let dirs = ["d1", "d1/d1", "d2", "d2/d2"];
    let symlinks = [("../../d2/d2", "d1/d1/l1"), ("../../d1/d1", "d2/d2/l2")];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir.path()).unwrap();


    let _: Vec<_> = DfsPathIterator::from_path(tmp_dir_path, true).unwrap()
      .map(|res| res.unwrap())
      .collect();
  },

  symlink_crazy_cycle_detected, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let dirs = ["d1", "d1/d1", "d2", "d2/d2", "d3", "d3/d3"];
    let symlinks = [("../../d2/d2", "d3/d3/l1"), ("../../d1/d1", "d2/d2/l2"), ("../../d3", "d1/d1/l3")];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir.path()).unwrap();

    DfsPathIterator::from_path(tmp_dir_path, true).unwrap()
      .for_each(|res| println!("crazypath: {}", res.unwrap().path().display()));
  },
  succeed_cp_run, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2/d1");

    let dirs = ["d1", "d1/d2", "d2"];
    let files = [("f1", "foo"), ("d1/f2", "bar"), ("d1/d2/f3", "baz")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });

    runner.run().unwrap();

    let src_iter = DfsPathIterator::from_path(&abs_d1, false).unwrap();
    let dst_iter = DfsPathIterator::from_path(&abs_d2, false).unwrap();
    for (src_res, dst_res) in src_iter.zip(dst_iter) {
      let (src_dir_entry, dst_dir_entry) = (src_res.unwrap(), dst_res.unwrap());
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        assert_eq!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }

  },

  succeed_cp_run_multiple_source_files, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_f1 = tmp_dir_path.join("d1/f1");
    let abs_f2 = tmp_dir_path.join("d1/f2");
    let abs_f3 = tmp_dir_path.join("d1/f3");

    let dirs = ["d1", "d2"];
    let files = [("d1/f1", "foo"), ("d1/f2", "bar"), ("d1/f3", "baz")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_f1.to_str().unwrap(), abs_f2.to_str().unwrap(), abs_f3.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: false,
      overwrite: false,
      follow_symlinks: false,
    });

    runner.run().unwrap();

    let src_iter = DfsPathIterator::from_path(&abs_d1, false).unwrap()
      .map(|res| res.unwrap());
    let dst_iter = DfsPathIterator::from_path(&abs_d2, false).unwrap()
      .map(|res| res.unwrap());
    for (src_dir_entry, dst_dir_entry) in src_iter.zip(dst_iter) {
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        assert_eq!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }

  },

  succeed_cp_run_multiple_source_dirs, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_d3 = tmp_dir_path.join("d3");
    let abs_dst_d1 = tmp_dir_path.join("d3/d1");
    let abs_dst_d2 = tmp_dir_path.join("d3/d2");

    let dirs = ["d1", "d1/d2", "d2", "d3"];
    let files = [("f1", "foo"), ("d1/f2", "bar"), ("d1/d2/f3", "baz"), ("d2/f4", "quux")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap(), abs_d2.to_str().unwrap()],
      dst_path: abs_d3.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });

    runner.run().unwrap();

    let mut src_vec = DfsPathIterator::from_path(&abs_d1, false).unwrap()
      .chain(DfsPathIterator::from_path(&abs_d2, false).unwrap())
      .map(|res| res.unwrap())
      .collect::<Vec<fs::DirEntry>>();
    let mut dst_vec = DfsPathIterator::from_path(&abs_dst_d1, false).unwrap()
      .chain(DfsPathIterator::from_path(&abs_dst_d2, false).unwrap())
      .map(|res| res.unwrap())
      .collect::<Vec<fs::DirEntry>>();

    src_vec.sort_by_key(|dir| dir.path());
    dst_vec.sort_by_key(|dir| dir.path());

    for (src_dir_entry, dst_dir_entry) in src_vec.iter().zip(dst_vec.iter()) {
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        assert_eq!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }
  },

  overwrite_files_non_recursive, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_f1 = tmp_dir_path.join("d1/f1");
    let abs_f2 = tmp_dir_path.join("d1/f2");

    let dirs = ["d1", "d2"];
    let files = [("d1/f1", "foo"), ("d1/f2", "bar"), ("d2/f1", "baz"), ("d2/f2", "quux")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_f1.to_str().unwrap(), abs_f2.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: false,
      overwrite: true,
      follow_symlinks: false,
    });

    runner.run().unwrap();

    let src_iter = DfsPathIterator::from_path(&abs_d1, false)
      .unwrap()
      .map(|res| res.unwrap());
    let dst_iter = DfsPathIterator::from_path(&abs_d2, false)
      .unwrap()
      .map(|res| res.unwrap());

    for (src_dir_entry, dst_dir_entry) in src_iter.zip(dst_iter) {
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        assert_eq!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }
  },

  overwrite_files_recursive, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2_d1 = tmp_dir_path.join("d2/d1");

    let dirs = ["d1", "d1/d3", "d2", "d2/d3"];
    let files = [("d1/f1", "foo"), ("d1/f2", "bar"), ("d1/d3/f3", "blah"),
                 ("d2/f1", "baz"), ("d2/f2", "quux"), ("d2/d3/f3", "blam")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2_d1.to_str().unwrap(),
      is_recursive: true,
      overwrite: true,
      follow_symlinks: false,
    });


    runner.run().unwrap();

    let src_iter = DfsPathIterator::from_path(&abs_d1, false)
      .unwrap()
      .map(|res| res.unwrap());
    let dst_iter = DfsPathIterator::from_path(&abs_d2_d1, false)
      .unwrap()
      .map(|res| res.unwrap());

    for (src_dir_entry, dst_dir_entry) in src_iter.zip(dst_iter) {
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        assert_eq!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }
  },

  dont_overwrite_files_non_recursive,  {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_f1 = tmp_dir_path.join("d1/f1");
    let abs_f2 = tmp_dir_path.join("d1/f2");

    let dirs = ["d1", "d2"];
    let files = [("d1/f1", "foo"), ("d1/f2", "bar"),
                 ("d2/f1", "baz"), ("d2/f2", "quux")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_f1.to_str().unwrap(), abs_f2.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: false,
      overwrite: false,
      follow_symlinks: false,
    });

    let mut err_vec = runner.run().unwrap_err();
    let errors = err_vec.errors();
    assert_eq!(errors.len(), 2);
    match errors.pop().unwrap().kind() {
      ErrorKind::FileExists(_) => {},
      _ => panic!("unexpected error"),
    }

    let src_iter = DfsPathIterator::from_path(&abs_d1, false)
      .unwrap()
      .map(|res| res.unwrap());
    let dst_iter = DfsPathIterator::from_path(&abs_d2, false)
      .unwrap()
      .map(|res| res.unwrap());

    for (src_dir_entry, dst_dir_entry) in src_iter.zip(dst_iter) {
      let (src_path, dst_path) = (src_dir_entry.path(), dst_dir_entry.path());
      if src_path.is_dir() {
        assert_eq!(dst_path.is_dir(), true);
      } else if src_path.is_file() {
        // file should not have been copied and hash should not match
        assert_ne!(hash_file(&src_path).unwrap(), hash_file(&dst_path).unwrap());
      }
    }
  },

  symlinkat_dir, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_l1 = tmp_dir_path.join("d2/l1");

    let dirs = ["d1", "d1/d2", "d1/d2/d3", "d1/d2/d3/d4", "d2"];
    let layout = FsLayout::new(&dirs[..], vec![], vec![]);
    layout.build(tmp_dir_path).unwrap();

    symlinkat("../d1/d2", (&abs_l1).to_str().unwrap()).unwrap();

    assert_eq!(fs::symlink_metadata(&abs_l1).unwrap().file_type().is_symlink(), true);
    assert_eq!(fs::read_link((&abs_l1).to_str().unwrap()).unwrap().to_str().unwrap(), "../d1/d2");

    let link_dir_contents: Vec<_> = abs_l1.read_dir()
      .unwrap()
      .map(|dir_entry| {
        dir_entry.unwrap().path()
      })
      .collect();

    assert_eq!(link_dir_contents, vec![tmp_dir_path.join("d2/l1/d3")])
  },

  symlinkat_file, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_f1 = tmp_dir_path.join("d1/f1");
    let abs_l1 = tmp_dir_path.join("d2/l1");

    let dirs = ["d1", "d2"];
    let files = [("d1/f1", "foo")];
    let layout = FsLayout::new(&dirs[..], &files[..], vec![]);
    layout.build(tmp_dir_path).unwrap();

    symlinkat("../d1/f1", (&abs_l1).to_str().unwrap()).unwrap();

    assert_eq!(fs::symlink_metadata(&abs_l1).unwrap().file_type().is_symlink(), true);
    assert_eq!(fs::read_link((&abs_l1).to_str().unwrap()).unwrap().to_str().unwrap(), "../d1/f1");
    assert_eq!(hash_file(abs_f1).unwrap(), hash_file(abs_l1).unwrap());
  },

  cp_symlink_dir_recursive, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_src_l1 = tmp_dir_path.join("d1/l1");
    let abs_dst_l1 = tmp_dir_path.join("d2/d1/l1");

    let dirs = ["d1", "d1/d2", "d1/d2/d3", "d1/d2/d3/d4", "d2"];
    let symlinks = [("d2/d3", abs_src_l1.to_str().unwrap())];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });

    print_tree(&tmp_dir_path);
    runner.run().unwrap();
    print_tree(&tmp_dir_path);

    assert_eq!(fs::symlink_metadata(&abs_dst_l1).unwrap().file_type().is_symlink(), true);
    assert_eq!(fs::read_link((&abs_dst_l1).to_str().unwrap()).unwrap().to_str().unwrap(), "d2/d3");

    let link_dir_contents: Vec<_> = abs_dst_l1.read_dir()
      .unwrap()
      .map(|dir_entry| {
        dir_entry.unwrap().path()
      })
      .collect();

    assert_eq!(link_dir_contents, vec![tmp_dir_path.join("d2/d1/l1/d4")])
  },

  cp_symlink_dir_non_recursive, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_src_l1 = tmp_dir_path.join("d1/l1");
    let abs_dst_l1 = tmp_dir_path.join("l1");

    let dirs = ["d1", "d1/d2", "d1/d2/d3", "d1/d2/d3/d4"];
    let symlinks = [("d2/d3", abs_src_l1.to_str().unwrap())];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_src_l1.to_str().unwrap()],
      dst_path: abs_dst_l1.to_str().unwrap(),
      is_recursive: false,
      overwrite: false,
      follow_symlinks: false,
    });

    runner.run().unwrap();

    assert_eq!(fs::symlink_metadata(&abs_dst_l1).unwrap().file_type().is_symlink(), true);
    assert_eq!(fs::read_link((&abs_dst_l1).to_str().unwrap()).unwrap().to_str().unwrap(), "d2/d3");

    abs_dst_l1.read_dir().unwrap_err();
  },

  cycle_no_follow_succeeds, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_src_l1 = tmp_dir_path.join("d1/d2/d3/d4/l1");
    let abs_dst_l1 = tmp_dir_path.join("d2/d1/d2/d3/d4/l1");

    let dirs = ["d1", "d1/d2", "d1/d2/d3", "d1/d2/d3/d4", "d2"];
    let symlinks = [("../../../..", abs_src_l1.to_str().unwrap())];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: true,
      overwrite: false,
      follow_symlinks: false,
    });

    print_tree(tmp_dir_path);
    runner.run().unwrap();
    print_tree(tmp_dir_path);

    assert_eq!(fs::symlink_metadata(&abs_dst_l1).unwrap().file_type().is_symlink(), true);
    assert_eq!(fs::read_link((&abs_dst_l1).to_str().unwrap()).unwrap().to_str().unwrap(), "../../../..");

    let link_dir_contents: Vec<_> = abs_dst_l1.read_dir()
      .unwrap()
      .map(|dir_entry| {
        dir_entry.unwrap().path()
      })
      .collect();

    assert_eq!(link_dir_contents, vec![tmp_dir_path.join("d2/d1/d2/d3/d4/l1/d1")])
  },

  cycle_follow_fails, {
    let tmp_dir = tempdir::TempDir::new("cp-test").unwrap();
    let tmp_dir_path = tmp_dir.path();

    let abs_d1 = tmp_dir_path.join("d1");
    let abs_d2 = tmp_dir_path.join("d2");
    let abs_src_l1 = tmp_dir_path.join("d1/d2/d3/d4/l1");
    let abs_dst_d2 = tmp_dir_path.join("d2/d1/d2/d3/d4/l1/d2");
    let abs_dst_d1 = tmp_dir_path.join("d2/d1/d2/d3/d4/l1/d2/d1");

    let dirs = ["d1", "d1/d2", "d1/d2/d3", "d1/d2/d3/d4", "d2"];
    let symlinks = [("../../../..", abs_src_l1.to_str().unwrap())];
    let layout = FsLayout::new(&dirs[..], vec![], &symlinks[..]);
    layout.build(tmp_dir_path).unwrap();

    let runner = Runner::new(Config{
      src_paths: vec![abs_d1.to_str().unwrap()],
      dst_path: abs_d2.to_str().unwrap(),
      is_recursive: true,
      overwrite: true,
      follow_symlinks: true,
    });

    print_tree(tmp_dir_path);
    let mut err_vec = runner.run().unwrap_err();
    let errors = err_vec.errors();
    assert_eq!(errors.len(), 1);
    match errors.pop().unwrap().kind() {
      ErrorKind::CopyDirIntoItself{..} => {},
      _ => panic!("unexpected ErrorKind"),
    }
    print_tree(tmp_dir_path);

    assert_eq!((&abs_dst_d2).exists(), true);
    assert_eq!((&abs_dst_d1).exists(), false);
  }
);
