extern crate libc;

use std::{
  path,
  result,
  error,
  env,
  io,
  fs,
  fmt,
  // os::unix::fs::{symlink as unix_symlink},
  os::{self, unix::io::AsRawFd, linux::fs::MetadataExt},
  ffi,
  collections,
  mem,
  convert,
};


fn cstr(p: &path::PathBuf) -> Result<ffi::CString> {
  match p.to_str() {
    Some(s) => Ok(ffi::CString::new(s)?),
    None => Err(Error::new_simple(ErrorKind::PathConversionError(p.clone().into_boxed_path()))),
  }
}

// this function is not technically needed, but fun
fn symlinkat<P: AsRef<path::Path>>(target: P, dst: P) -> Result<()> {
  // it seems like it is necessary to change directories
  // figure out if there's a -r equivalent, which would make this process less error prone
  // thank changing directories to the dst parent dir every time
  let target_path_buf = target.as_ref().to_path_buf();
  let dst_path_buf = dst.as_ref().to_path_buf();

  let link_cstring = cstr(&target_path_buf)?;
  let dst_cstring = cstr(&dst_path_buf)?;
  let parent_dir_fd: libc::c_int = match dst_path_buf.parent() {
    Some(parent_path) => fs::File::open(parent_path)?.as_raw_fd(),
    None => return Err(
      Error::new_simple(ErrorKind::PathErrorNoParent(dst_path_buf.clone().into_boxed_path()))),
  };

  unsafe {
    if libc::symlinkat(link_cstring.as_ptr(), parent_dir_fd, dst_cstring.as_ptr()) != 0 {
      Err(io::Error::last_os_error().into())
    } else {
      Ok(())
    }
  }
}

#[derive(Debug)]
pub struct Error {
  repr: Repr,
}

#[derive(Debug)]
enum Repr {
  Simple(ErrorKind),
  Custom(Box<Custom>),
}

#[derive(Debug)]
pub enum ErrorKind {
  NotRecursiveSrcIsDir,
  IoError,
  FfiError,
  PathError,
  PathErrorNoParent(Box<path::Path>),
  PathConversionError(Box<path::Path>),
  FileWithNoFileName,
  FileExists(Box<path::Path>),
  RecursiveCopyToFileDestination,
  InconsistentStacks,
  BaseIsSymlink,
  CopyDirIntoItself{src: Box<path::Path>, dst: Box<path::Path>},
  UnexpectedFileType,
}

impl ErrorKind {
  // Should refactor to return String so you can format with further information.
  fn as_str(&self) -> &'static str {
    match *self {
      ErrorKind::NotRecursiveSrcIsDir => "-r not specified, src is directory",
      ErrorKind::IoError => "io error",
      ErrorKind::FfiError => "ffi error",
      ErrorKind::PathError => "path error",
      ErrorKind::PathErrorNoParent(_) => "no parent",
      ErrorKind::PathConversionError(_) => "path conversion error",
      ErrorKind::FileWithNoFileName => "file path has no name (is it '/'?)",
      ErrorKind::FileExists(_) => "file exists and overwrite is disabled",
      ErrorKind::RecursiveCopyToFileDestination => "recursive copy destination must be a directory",
      ErrorKind::InconsistentStacks => "inconsistent stacks",
      ErrorKind::BaseIsSymlink => "base is a symlink",
      ErrorKind::CopyDirIntoItself{..} => "cannot copy dir into itself",
      ErrorKind::UnexpectedFileType => "unexpected file type",
    }
  }
}

#[derive(Debug)]
struct Custom {
  kind: ErrorKind,
  error: Box<error::Error+Send+Sync>,
}

impl Error {
  fn new<E>(kind: ErrorKind, error: E) -> Error
  where E: Into<Box<error::Error+Send+Sync>>
  {
    Error{
      repr: Repr::Custom(Box::new(Custom{
        kind: kind,
        error: error.into(),
      }))
    }
  }

  fn new_simple(kind: ErrorKind) -> Error {
    Error{
      repr: Repr::Simple(kind),
    }
  }

  // wondering if there's a resonable way to return an io::ErrorKind as well.
  fn kind(&mut self) -> &ErrorKind {
    match self.repr {
      Repr::Simple(ref kind) => kind,
      Repr::Custom(ref boxed) => match **boxed {
        Custom{ref kind, ..} => kind,
      },
    }
  }
}

impl fmt::Display for Error {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match &self.repr {
      Repr::Simple(kind) => {
        match kind {
          ErrorKind::PathErrorNoParent(path) => {
            write!(f, "path has no parent: {}", path.display())
          },
          ErrorKind::PathConversionError(path) => {
            write!(f, "could not convert path to cstring: {}", path.display())
          },
          ErrorKind::FileExists(path) => {
            writeln!(f, "file exists and overwrite is disabled: {}", path.display())
          },
          ErrorKind::CopyDirIntoItself{src, dst} => {
            write!(f, "cannot copy a directory, '{}', into itself, '{}'", src.display(), dst.display())
          },
          _ => write!(f, "{}", kind.as_str()),
        }
      },
      Repr::Custom(boxed) => write!(f, "{}: {}", &*boxed.kind.as_str(), &*boxed.error),
    }
  }
}

impl error::Error for Error {
  fn description(&self) -> &str {
    match self.repr {
      Repr::Simple(ref kind) => kind.as_str(),
      Repr::Custom(ref boxed) => {
        (**boxed).kind.as_str()
      },
    }
  }

  fn cause(&self) -> Option<&error::Error> {
    match self.repr {
      Repr::Simple(_) => None,
      Repr::Custom(ref boxed) => {
        Some(&*(**boxed).error)
      }
    }
  }
}

impl From<io::Error> for Error {
  fn from(error: io::Error) -> Self {
    Error::new(ErrorKind::IoError, error)
  }
}

impl From<path::StripPrefixError> for Error {
  fn from(error: path::StripPrefixError) -> Self {
    Error::new(ErrorKind::PathError, error)
  }
}

impl From<ffi::NulError> for Error {
  fn from(error: ffi::NulError) -> Self {
    Error::new(ErrorKind::FfiError, error)
  }
}

pub struct ErrVec(Vec<Error>);

impl ErrVec {
  fn new() -> ErrVec {
    ErrVec(Vec::new())
  }

  fn push<E: convert::Into<Error>>(&mut self, error: E) {
    self.0.push(error.into());
  }

  fn result(self) -> RunResult<()> {
    if self.0.len() == 0 {
      Ok(())
    } else {
      Err(self)
    }
  }

  pub fn errors(&mut self) -> &mut Vec<Error> {
    &mut self.0
  }

}

impl fmt::Debug for ErrVec {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "[{}]",
           self.0.iter()
           .map(|e| format!("'{}'", e))
           .collect::<Vec<String>>()
           .join(", "))
  }
}

impl From<Error> for ErrVec {
  fn from(error: Error) -> Self {
    ErrVec(vec![error])
  }
}

impl From<io::Error> for ErrVec {
  fn from(error: io::Error) -> Self {
    ErrVec(vec![convert::From::from(error)])
  }
}

pub type Result<T> = result::Result<T, Error>;
pub type RunResult<T> = result::Result<T, ErrVec>;

#[derive(Debug)]
pub struct Config<'a> {
  pub src_paths: Vec<&'a str>,
  pub dst_path: &'a str,
  pub is_recursive: bool,
  pub overwrite: bool,
  pub follow_symlinks: bool,
}

pub struct Runner<'a> {
  cfg: Config<'a>,
  created_inodes: collections::HashSet<u64>,
}

impl<'a> Runner<'a> {
  pub fn new(cfg: Config<'a>) -> Runner<'a> {
    Runner{
      cfg: cfg,
      created_inodes: collections::HashSet::new(),
    }
  }

  fn copy_file(&self, src_path: &path::Path, dst_path: &path::Path) -> Result<()> {
    let src_file_type = fs::symlink_metadata(src_path)?.file_type();

    if src_file_type.is_dir() {
      return Err(Error::new_simple(ErrorKind::NotRecursiveSrcIsDir));
    }

    let dst_file_path = if dst_path.is_dir() {
      if let Some(file_name) = src_path.file_name() {
        dst_path.join(file_name)
      } else {
        return Err(Error::new_simple(ErrorKind::FileWithNoFileName));
      }
    } else {
      dst_path.to_path_buf()
    };

    if dst_file_path.exists() && !self.cfg.overwrite {
      return Err(Error::new_simple(ErrorKind::FileExists(dst_file_path.into_boxed_path())));
    } else if src_file_type.is_symlink() && !self.cfg.follow_symlinks {
      let link_target_path = fs::read_link(src_path)?;
      if link_target_path.is_relative() {
        symlinkat(&link_target_path, &dst_file_path)?;
      } else {
        // Might need to figure out cp semantics for when the absolute pointer
        // lies under the src_path, but it seems like absolute means absolute.
        os::unix::fs::symlink(link_target_path, dst_file_path)?;
      }
    } else {
      fs::copy(src_path, dst_file_path)?;
    }
    Ok(())
  }

  fn copy_file_recursive(&mut self, src_path: &path::Path, base_path: &path::Path) -> Result<()> {
    // what if this is a symlink?
    let dst_path = if base_path.is_dir() {
      if let Some(file_name) = src_path.file_name() {
        base_path.join(file_name)
      } else {
        return Err(Error::new_simple(ErrorKind::FileWithNoFileName));
      }
    } else if base_path.is_file() {
      return Err(Error::new_simple(ErrorKind::RecursiveCopyToFileDestination))
    } else {
      base_path.to_path_buf()
    };

    fs::create_dir(&dst_path)?;
    self.created_inodes.insert(fs::symlink_metadata(&dst_path)?.st_ino());

    for res in DfsPathIterator::from_path(&src_path, self.cfg.follow_symlinks)? {
      let src_dir_entry = res?;
      let cur_src = src_dir_entry.path();
      let src_symlink_metadata = fs::symlink_metadata(&cur_src)?;
      let src_symlink_file_type = src_symlink_metadata.file_type();
      let src_deref_metadata = (&cur_src).metadata()?;
      let src_deref_file_type = src_deref_metadata.file_type();
      let suffix = cur_src.strip_prefix(&src_path)?;
      let cur_dst = dst_path.join(suffix);
      // move this into a function on its own and see if you can dedupe with regular copy_file
      // catch error and print but continue on

      if self.cfg.follow_symlinks {
        if let Some(_) = self.created_inodes.get(&src_symlink_metadata.st_ino()) {
          return Err(Error::new_simple(ErrorKind::CopyDirIntoItself{
            src: cur_src.clone().into_boxed_path(),
            dst: cur_dst.into_boxed_path(),
          }));
        }
      }

      if src_symlink_file_type.is_symlink() && !self.cfg.follow_symlinks {
        // TODO does overwrite need to be checked?
        let link_target = fs::read_link(&cur_src)?;
        symlinkat(link_target, cur_dst.to_path_buf())?;
      } else if src_symlink_file_type.is_file() || (self.cfg.follow_symlinks && src_deref_file_type.is_file()) {
        if cur_dst.exists() && !self.cfg.overwrite {
          info!("'{}' exists: skipping", cur_dst.display());
          continue
        }
        fs::copy(&cur_src, &cur_dst)?;
      } else if src_symlink_file_type.is_dir() || (self.cfg.follow_symlinks && src_deref_file_type.is_dir()) {
        // TODO does overwrite need to be checked?
        fs::create_dir(&cur_dst)?;
      } else {
        return Err(Error::new_simple(ErrorKind::UnexpectedFileType));
      }
      self.created_inodes.insert(fs::symlink_metadata(&cur_dst)?.st_ino());
      info!("'{}' -> '{}'", cur_src.display(), cur_dst.display());
    };

    Ok(())
  }

  pub fn run(mut self) -> RunResult<()> {
    let cwd = env::current_dir()?;
    let src_paths: Vec<_> = self.cfg.src_paths
      .iter()
      .map(|src| {
        let src_path = path::PathBuf::new().join(src);
        if src_path.is_relative() {
          cwd.join(src_path)
        } else {
          src_path
        }
      }).collect();


    let mut dst_path = path::PathBuf::new().join(&self.cfg.dst_path);

    if dst_path.is_relative() {
      dst_path = cwd.join(dst_path);
    }

    let mut err_vec = ErrVec::new();
    for src_path in src_paths {
      if self.cfg.is_recursive {
        match self.copy_file_recursive(src_path.as_path(), dst_path.as_path()) {
          Ok(_) => {},
          // should dynamically read program name from args
          Err(error) => {
            error!("cp: {}", error);
            err_vec.push(error);
          },
        }
      } else {
        // TODO make this arm just print errors instead of re-raising them and
        // continue through all src_paths even on error
        match self.copy_file(src_path.as_path(), dst_path.as_path()) {
          Ok(_) => {},
          Err(error) => {
            error!("cp: {}", error);
            err_vec.push(error);
          },
        }
      }
    }

    err_vec.result()
  }
}

struct DfsPathIterator {
  dir_stack: Vec<fs::ReadDir>,
  inodes_stack: Vec<collections::HashSet<u64>>,
  inodes: collections::HashSet<u64>,
  follow_symlinks: bool,
}

impl DfsPathIterator {
  fn from_path<P: AsRef<path::Path>>(path: P, follow_symlinks: bool) -> Result<DfsPathIterator> {
    let path =  path.as_ref();
    let symlink_metadata = fs::symlink_metadata(path)?;

    // We'll find out if its not a dir on the read_dir below so this check is
    // sufficient
    if symlink_metadata.file_type().is_symlink() && !follow_symlinks {
      // TODO add a path to BaseIsSymlink and give it a custom format error message
      return Err(Error::new_simple(ErrorKind::BaseIsSymlink))
    }

    let symlink_ino = symlink_metadata.st_ino();
    let deref_metadata = path.metadata()?;
    let deref_ino = deref_metadata.st_ino();

    let mut set = collections::HashSet::new();
    set.extend(&[symlink_ino, deref_ino]);

    Ok(DfsPathIterator{
      dir_stack: vec![path.read_dir()?],
      inodes_stack: vec![collections::HashSet::new()],
      inodes: set,
      follow_symlinks: follow_symlinks,
    })
  }

  fn record_inodes<'a, I: IntoIterator<Item=&'a u64>>(&mut self, new_inodes: I) -> Result<()> {
    if let Some(mut inodes) = self.inodes_stack.pop() {
      inodes.extend(new_inodes);
      self.inodes_stack.push(inodes);
      Ok(())
    } else {
      Err(Error::new_simple(ErrorKind::InconsistentStacks))
    }
  }

  fn push_inodes(&mut self) {
    self.inodes_stack.push(collections::HashSet::new());
  }

  fn pop_inodes(&mut self) -> Result<()> {
    if let Some(inodes) = self.inodes_stack.pop() {
      let diff = self.inodes.difference(&inodes).map(|&x| x).collect();
      mem::replace(&mut self.inodes, diff);
      Ok(())
    } else {
      Err(Error::new_simple(ErrorKind::InconsistentStacks))
    }
  }

  // shouldn't use deref_metadata when we aren't following symlinks


  // hard links aren't allowed for directories so I think that the inode cycle
  // detection method is valid instead of something like Floyd's algorithm.
  fn try_next(&mut self) -> Result<Option<fs::DirEntry>> {
    while let Some(mut iterator) = self.dir_stack.pop() {
      if let Some(next) = iterator.next() {
        self.dir_stack.push(iterator);
        let dir_entry = next?;
        let symlink_metadata = fs::symlink_metadata(dir_entry.path())?;
        let symlink_file_type = symlink_metadata.file_type();
        let symlink_ino = symlink_metadata.st_ino();
        let deref_metadata = dir_entry.path().metadata()?;
        let deref_file_type = deref_metadata.file_type();
        let deref_ino = deref_metadata.st_ino();
        let inos = if deref_ino == symlink_ino || !self.follow_symlinks {
          vec![symlink_ino]
        } else {
          // is it really necessary to follow both? It seems like its symlink
          // or deref but both is redundant information.
          vec![symlink_ino, deref_ino]
        };
        self.record_inodes(&inos)?;
        trace!("path: {}, canonical path: {}, symlink_ino: {}, deref_ino: {}, hash_set: {:?}",
               dir_entry.path().display(), dir_entry.path().canonicalize()?.display(),
               symlink_metadata.st_ino(), deref_ino, self.inodes);
        if deref_file_type.is_file() {
          // file, do nothing
        } else if !inos.iter().all(|&ino| self.inodes.insert(ino)) {
          // should return error instead
          warn!("cycle detected: '{}'", dir_entry.path().display());
          // This continue prevents this path from being returned since the
          // configuration indicates that we are to dereference symlinks but
          // can't since it's a cycle.
          continue
        } else if symlink_file_type.is_dir() || (symlink_file_type.is_symlink() && self.follow_symlinks) {
          self.dir_stack.push(dir_entry.path().read_dir()?);
          self.push_inodes();
        }
        return Ok(Some(dir_entry));
      } else {
        self.pop_inodes()?;
      }
    }
    Ok(None)
  }
}

impl Iterator for DfsPathIterator
{
  type Item = Result<fs::DirEntry>;

  fn next(&mut self) -> Option<Self::Item> {
    match self.try_next() {
      Ok(Some(dir_entry)) => Some(Ok(dir_entry)),
      Ok(None) => None,
      Err(err) => Some(Err(err)),
    }
  }
}

#[cfg(test)]
mod tests;
